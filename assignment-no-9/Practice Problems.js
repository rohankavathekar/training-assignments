/*Creating closure*/
function slang(name) {
    console.log(`${this.slang()} ${name}`);
}

var india = {
    slang() {
        return 'Namastey';
    }
}
var america = {
    slang() {
        return 'Howdy';
    }
}
slang.call(india, "Donald");
slang.call(america, "Modi");


/*bind*/
function bindMessage(firstName, secondName, middleName) {
    console.log(`${this.bindMessage()} ${firstName} ${middleName} ${secondName}`);
}
const firstName = 'Alex';
const secondName = 'Perry';
const middleName = 'James';
var morning = {
    bindMessage() {
        return 'Good Morning';
    }
}
var evening = {
    bindMessage() {
        return 'Good Evening';
    }
}

var boundFunc = bindMessage.bind(morning, firstName);
boundFunc(secondName);
