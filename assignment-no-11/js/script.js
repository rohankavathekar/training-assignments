let switchBoard = document.querySelector(".board");
let status = document.querySelector(".status");
let helloText = document.querySelector('.hello');
let button = document.querySelector(".switch");
let capture = document.querySelector(".capture");
let row1 = document.querySelector(".row1")
let row2 = document.querySelector(".row2")
let row3 = document.querySelector(".row3")
let row4 = document.querySelector(".row4")
/*manupulating css data*/

button.addEventListener("click", function() {
  switchBoard.classList.toggle("grey");
  row1.classList.toggle("odd");
  row2.classList.toggle("even");
  row3.classList.toggle("odd");
  row4.classList.toggle("even");
  });

/*manupulating html data*/
function toggle() {
if (status.innerHTML == "OFF" && helloText.innerHTML == "" ) {
    status.innerHTML = "ON";
   helloText.innerHTML = "Hello for 5 seconds";

  } else {
  status.innerHTML = "OFF";
  helloText.innerHTML = "";
  }
}
function useCapture() {
  if(capture.checked == true) {
    button.style.backgroundColor="#00ff00";
      /*capture*/
    document.getElementById("one").addEventListener("click",function()
    {console.log("1");},true);
    document.getElementById("two").addEventListener("click",function()
    {console.log("2");},true);
    document.getElementById("three").addEventListener("click",function()
    {console.log("3");},true);
    alert("Capture Mode = ON [ Check console Panel and click switch]")
  }
  else {
    button.style.backgroundColor="#ffff00"
  }
}
/*hide text after 5 seconds*/
setInterval(function () {
helloText.style.visibility='hidden'}, 5000);
