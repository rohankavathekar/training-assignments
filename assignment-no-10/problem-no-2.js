function Animal (name,age) {
  this.name = name;
  this.age = age;
}
Animal.prototype.eat = function() {
  console.log(`${this.age} Years Old ${this.name} eats`);
}

function Human (name,age,earning) {
  Animal.call(this,name,age);
  this.earning = earning;
}
Human.prototype = Object.create(Animal.prototype);
Human.prototype.constructor = Human;

Human.prototype.salary = function() {
  console.log(`${this.age} Years Old ${this.name} earns ${this.earning} `);
}

function Dog (name,age) {
  Animal.call(this,name,age);
}
Dog.prototype = Object.create(Animal.prototype);
Dog.prototype.constructor = Human;

Dog.prototype.bark = function() {
  console.log(`${this.age} Years Old ${this.name} Barks Alot!`);
}

const person = new Human("Kabir",21,1000);
const Doggie = new Human("Priti",8);
