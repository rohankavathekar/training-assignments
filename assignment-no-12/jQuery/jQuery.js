$(document).ready(function(){
  //getting values
 var e,
 switchBoard = {
   settings: {
    board : "#board",
    status : "#status",
    helloText : "#hello",
    button : "#switch",
    capture : "#capture",
    row1 : ".row1",
    row2 : ".row2"
   },

   init:function(){
     e = this.settings;
     this.bindUIActions();
   },

   bindUIActions: function() {
    $(document).on('click',e.button,this.toggles);
    $(document).on('click',e.capture,this.useCapture);
   },

   toggles:function() {
    $(e.board).toggleClass('grey');//changes board color
    $(e.row1).toggleClass('even');
    $(e.row2).toggleClass('odd');
    $(e.status).text($(e.status).text() == 'OFF' ? 'ON' : 'OFF');//changes status ON/OFF
    $(e.helloText).text($(e.helloText).text() == '' ? 'Hello for 5 Seconds' : '');//message
   },
   //function for capture
    useCapture: function() {
    $(e.button).toggleClass('green');
   },
 }
 switchBoard.init();

  $("#sort, #drop").sortable({ connectWith: '#sort, #drop',
    receive: function() {

      if($('#drop').children().length > 3) {
         $('#sort').sortable('cancel')
      }
      else {
        $('#drop').children().addClass('dropped');
        $('#drop').find( "span" ).html( "Dropped!" );
      }
    }
    });

  $( "#drop" ).droppable({
    drop: function() {
      $('#drop').addClass("bg-change").children();

    }
  });


});