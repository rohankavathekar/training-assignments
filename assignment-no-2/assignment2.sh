#!/bin/sh
START=1
RANDOM=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)  #Creates random characters
echo "Enter FolderName"
read FolderName                             #User Input For FolderName
echo "Enter Number of files to be created"
read END                                    #User Input For files quantity
mkdir $FolderName                            #Create Folder
cd $FolderName
for (( index=$START; index<=$END; index++ ))
do
  echo "File $RANDOM" > $FolderName$index.txt        #creating files with random text
done
cd ..
zip $FolderName.zip $FolderName/*            #zip folder
ReverseName=$(echo $FolderName | rev)         #reverse name and storing into variable
mkdir $ReverseName
cd $ReverseName
unzip ../$FolderName.zip                      #Unzip into name reversed folder
cd $FolderName
mv * ..                                      #moving all files one level up
cd ..
rmdir $FolderName                             #delete the empty folder
for (( index=$START; index<=$END; index++ ))
do
  remainder=$(($index % 2))
  if [ "$remainder"  -ne "0" ]; then          #give readonly acces to odd numbered files
    chmod 0400 $FolderName$index.txt
  fi
done


